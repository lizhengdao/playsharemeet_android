package com.zanma.playsharemeet.retrofit

import retrofit2.Call
import retrofit2.http.GET

interface Version {
    @GET("api/version")
    fun getVersion(): Call<String>
}